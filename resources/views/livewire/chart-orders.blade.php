<div wire:ignore x-data="{
    selectedYear: @entangle('selectedYear'),
    thisYearOrders: @entangle('thisYearOrders'),
    lastYearOrders: @entangle('lastYearOrders'),
    init() {
        const ctx = this.$refs.canvas;

        // add labels from January to December
        const labels = Array.from({ length: 12 }, (_, i) => {
            return new Date(2021, i, 1).toLocaleString('default', { month: 'long' });
        });

        const chartOrders = new Chart(ctx, {
            type: 'bar',
            data: {
                labels,
                datasets: [{
                    label: `${this.selectedYear - 1} Orders`,
                    data: this.lastYearOrders,
                    borderWidth: 1
                }, {
                    label: `${this.selectedYear} Orders`,
                    data: this.thisYearOrders,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        Livewire.on('updateTheChart', () => {
            {{-- @TODO - Update the values of the chart --}}
            chartOrders.data.datasets[0].data = this.lastYearOrders;
            chartOrders.data.datasets[1].data = this.thisYearOrders;

            {{-- @TODO - Update the chart labels --}}
            chartOrders.data.datasets[0].label = `${this.selectedYear - 1} Orders`;
            chartOrders.data.datasets[1].label = `${this.selectedYear} Orders`;

            chartOrders.update();
        });
    }
}" class="mt-4">
    <span>Year: </span>
    <select wire:change="updateOrdersCount" wire:model="selectedYear" id="selectedYear" name="selectedYear" class="border">
        @foreach ($availableYears as $year)
            <option value="{{ $year }}">{{ $year }}</option>
        @endforeach
    </select>
    <div>Selected Year: <span x-text="selectedYear"></span></div>
    <div class="my-6">
        <div><span x-text="selectedYear - 1"></span> Orders: <span
                x-text="lastYearOrders.reduce((a, b) => a + b)"></span></div>
        <div><span x-text="selectedYear"></span> Orders: <span x-text="thisYearOrders.reduce((a, b) => a + b)"></span>
        </div>
    </div>
    <canvas x-ref="canvas" id="myChart"></canvas>
</div>
