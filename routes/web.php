<?php

use App\Http\Requests\PostFormRequest;
use App\Models\Announcement;
use App\Models\Order;
use App\Models\Post;
use App\Models\Song;
use App\Models\User;
use Illuminate\Http\Client\Pool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/charts', function () {
    return view('charts');
});

Route::get('/stats', function () {
    // Create a NumberFormatter instance for en-US locale and currency format
    $formatter = new NumberFormatter('en-US', NumberFormatter::CURRENCY);

    $ordersCount = Order::where('created_at', '>=', now()->subDays(30))->count();
    $revenue = Order::where('created_at', '>=', now()->subDays(30))->sum('total');
    $usersCount = User::where('created_at', '>=', now()->subDays(30))->count();

    // Format the revenue as a currency
    $formattedRevenue = $formatter->formatCurrency($revenue / 100, 'USD');

    return view('stats', [
        'ordersCount' => $ordersCount,
        'revenue' => $formattedRevenue,
        'usersCount' => $usersCount,
    ]);
});
